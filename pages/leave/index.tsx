import Head from 'next/head'
import React, { FC, PureComponent } from 'react'
import Button from '@mui/material/Button';
import axios from 'axios';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import DateRangePicker from '@mui/lab/DateRangePicker';
// import AdapterDateFns from '@mui/lab/AdapterDateFns';
// import LocalizationProvider from '@mui/lab/LocalizationProvider';

import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';

import thLocale from 'date-fns/locale/th';

import Box from '@mui/material/Box';
import ms from 'ms'

interface Props {
    moduleId: string
}


const host = 'http://localhost:3000/'

const Index: FC<Props> = (props: Props) => {

    const [value, setValue] = React.useState(null);
    const [age, setAge] = React.useState(null);

    const [startDate, setStartDate] = React.useState(null);
    const [endDate, setEndDate] = React.useState(null);

    // const Index: FC<Props> = (props: Props) => {
    // class LeaveComponent extends PureComponent {
    // static propTypes = {

    // }
    // state = {
    //     age: null,
    //     value: null,
    //     startData: null,
    //     endData: null,
    // }

    const handleClick = (event): void => {
        axios.get(host + 'leave')
            .then(function (response) {
                // handle success
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });

    }


    const handleChange = (value) => {
        //  alert('Change')
        console.log('value ', value)
        //  this.setState({ age: value })
    }

    //  render() {
    // let { age, value, startData, endData } = this.state
    return (
        <div className='container-module-detail'>


            <TextField id="standard-basic" label="รหัสพนักงาน" variant="standard" />
            <TextField id="standard-basic" label="ชื่อพนักงาน" variant="standard" />

            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">ประเภทการลา</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={age}
                    label="Age"
                    onChange={handleChange}
                    variant="standard"
                >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                </Select>
            </FormControl>



            <TextField id="standard-basic" label="สาเหตุการลา" variant="standard" />


            <LocalizationProvider variant="standard" dateAdapter={AdapterDateFns} locale={thLocale}>
                <DatePicker
                    label="Start Date"
                    value={startDate}
                    
                    onChange={(newValue) => {
                       // console.log('start  ', newValue)
                        setStartDate(newValue);
                    }}
                    renderInput={(params) => <TextField {...params} />}
                />
            </LocalizationProvider>


            <LocalizationProvider variant="standard" dateAdapter={AdapterDateFns} locale={thLocale}>
                <DatePicker
                    label="End Date"
                    value={endDate}
                    onChange={(newValue) => {
                       // console.log('end  ', newValue)
                        setEndDate(newValue);
                    }}
                    renderInput={(params) => <TextField {...params} />}
                />
            </LocalizationProvider>



            <Button onClick={handleClick} variant="contained">Hello World</Button>

        </div>
    )
    // }
}

export default Index

// export default LeaveComponent;

